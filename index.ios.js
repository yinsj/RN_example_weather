/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import {
  AppRegistry
} from 'react-native';
import Weather from './js/WeatherComponent'

AppRegistry.registerComponent('Weather', () => Weather);
