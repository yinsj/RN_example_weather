/**
 * Created by yinsj on 2017/8/1.
 */
"use strict";

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ListView,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import weathers, { url, mapJsonToWeatherArray } from './weatherAPI'

export default class Weather extends Component {
  constructor(props) {
    super(props);

    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: undefined,
      isLoaded: false
    }
  }

  componentWillMount() {
    fetch(url).then(res => res.json()).then(json => this.setState({ isLoaded: true, dataSource: this.ds.cloneWithRows(mapJsonToWeatherArray(json))}))
  }

  render() {
    if (this.state.isLoaded) {
      return (
          <View style={styles.container}>
            <ListView dataSource={ this.state.dataSource }
                      renderRow={ (rowData, sectionIndex, rowIndex) => <Row rowData={ rowData } index={ rowIndex }/> }
                      style={ styles.listView }
            />
          </View>
      );
    } else {
      return (
          <View style={ styles.container }>
            <ActivityIndicator />
          </View>
      );
    }
  }
}

class Row extends Component {
  render() {
    let data = this.props.rowData;
    return (
        <View style={ styles.cellContainer }>
          <Text>{ '星期' + weekday[(new Date().getDay() + parseInt(this.props.index))%7] }</Text>
          <Icon name={ data.icon } size={ 20 } />
          <Text>{ data.wind }</Text>
          <Text>{ data.temperatureMax }</Text>
          <Text>{ data.temperatureMin }</Text>
        </View>
    );
  }
}

const weekday = ['日', '一', '二', '三', '四', '五', '六'];

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#F5FCFF',
  },
  listView: {
    paddingTop: 40,
    backgroundColor: 'beige'
  },
  cellContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 44,
    paddingHorizontal: 20,
  },

});