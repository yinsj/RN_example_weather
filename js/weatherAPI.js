/**
 * Created by yinsj on 2017/7/31.
 */
"use strict";

// var format={fa:图片1,fb：图片2,fc:温度1,fd：温度2,fe:风向1,ff：风向2,fg:风力1,fh：风力2,fi:日出日落};
//定义天气类型
const weatherName = {"00":"晴","01":"多云","02":"阴","03":"阵雨","04":"雷阵雨","05":"雷阵雨伴有冰雹","06":"雨夹雪","07":"小雨","08":"中雨","09":"大雨","10":"暴雨","11":"大暴雨","12":"特大暴雨","13":"阵雪","14":"小雪","15":"中雪","16":"大雪","17":"暴雪","18":"雾","19":"冻雨","20":"沙尘暴","21":"小到中雨","22":"中到大雨","23":"大到暴雨","24":"暴雨到大暴雨","25":"大暴雨到特大暴雨","26":"小到中雪","27":"中到大雪","28":"大到暴雪","29":"浮尘","30":"扬沙","31":"强沙尘暴","53":"霾","99":""};
const weatherIcon = { "00": "weather-sunny", "01": "weather-partlycloudy", "02": "weather-cloudy","03": "weather-lightning-rainy",  "04": "weather-lightning-rainy", "05": "weather-hail", "06": "weather-snowy-rainy", "07": "weather-rainy", "08": "weather-rainy", "09": "weather-pouring", "10": "weather-pouring", "11": "weather-pouring", "12": "weather-pouring", "13": "weather-snowy", "14": "weather-snowy", "15": "weather-snowy", "16": "weather-snowy","17": "weather-snowy",  "18": "weather-fog", "19": "weather-hail"};

//定义风向数组
const fxArr={"0":"无持续风向","1":"东北风","2":"东风","3":"东南风","4":"南风","5":"西南风","6":"西风","7":"西北风","8":"北风","9":"旋转风"};
//定义风力数组
const flArr={"0":"微风","1":"3-4级","2":"4-5级","3":"5-6级","4":"6-7级","5":"7-8级","6":"8-9级","7":"9-10级","8":"10-11级","9":"11-12级"};

export const url = 'http://mobile.weather.com.cn/data/forecast/101280601.html';

const mockJson = {
  "c": {
    "c1":"101280601",
    "c2":"shenzhen",
    "c3":"深圳",
    "c4":"shenzhen",
    "c5":"深圳",
    "c6":"guangdong",
    "c7":"广东",
    "c8":"china",
    "c9":"中国",
    "c10":"2",
    "c11":"0755",
    "c12":"518001",
    "c13":"114.109",
    "c14":"22.544",
    "c15":"40",
    "c16":"AZ9755","c17":"+8"
  },
  "f":{
    "f1":[
      {"fa":"01","fb":"01","fc":"32","fd":"25","fe":"0","ff":"0","fg":"0","fh":"0","fi":"06:18|18:01"},
      {"fa":"01","fb":"01","fc":"31","fd":"25","fe":"0","ff":"0","fg":"0","fh":"0","fi":"06:19|18:00"},
      {"fa":"07","fb":"07","fc":"27","fd":"24","fe":"0","ff":"0","fg":"0","fh":"0","fi":"06:19|17:59"},
      {"fa":"07","fb":"07","fc":"25","fd":"21","fe":"0","ff":"0","fg":"0","fh":"0","fi":"06:20|17:58"},
      {"fa":"07","fb":"02","fc":"23","fd":"19","fe":"0","ff":"0","fg":"0","fh":"0","fi":"06:20|17:57"},
      {"fa":"02","fb":"02","fc":"23","fd":"20","fe":"0","ff":"0","fg":"0","fh":"0","fi":"06:20|17:56"},
      {"fa":"02","fb":"02","fc":"25","fd":"22","fe":"0","ff":"0","fg":"0","fh":"0","fi":"06:21|17:56"}
      ],
    "f0":"201310121100"
  }
};

export const mapJsonToWeatherArray = json => [ ...json.f.f1.map( item => ({
  name: weatherName[item.fa],
  icon: weatherIcon[item.fa],
  temperatureMax: item.fc,
  temperatureMin: item.fd,
  wind: flArr[item.fg]
})) ];

export default mapJsonToWeatherArray(mockJson)